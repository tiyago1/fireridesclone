﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Pool;

[System.Serializable]
public class MapSettings
{
    public int ChunkLimit;

    public Color lightColor;
    public Color darkColor;
}

public struct Chunk
{
    public List<Vector3> UpWalls;
    public List<Vector3> DownWalls;

    public float LastX;
    public float LastY;

    public float Space;
}

public class MapGenerator : MonoBehaviour
{
    [SerializeField] private GameObject WallPrefab;
    [SerializeField] private MapSettings mapSettings;

    [SerializeField] private float multiplier;

    private float lastX;
    private float lastY;
    [SerializeField] private GameObject chunkPrefab;

    private Pool<ChunkElement> chunkPool;
    private ChunkElement lastChunk;

    private void Awake()
    {
        Initialize();
    }

    private void Initialize()
    {
        chunkPool = new Pool<ChunkElement>(new PrefabFactory<ChunkElement>(chunkPrefab), 2);
        CreateNewChunk();
    }

    public void CreateNewChunk()
    {
        ChunkElement chunkElement = chunkPool.Allocate();
        Chunk newChunk = Generate(mapSettings, lastX, lastY);
        lastX = newChunk.LastX;
        lastY = newChunk.LastY;

        chunkElement.SetupChunk(newChunk, mapSettings);
        if (lastChunk != null)
            chunkPool.Release(lastChunk);
        lastChunk = chunkElement;
    }

    private Chunk Generate(MapSettings settings, float currentX, float currentY)
    {
        Chunk wallConfig = new Chunk();
        wallConfig.UpWalls = new List<Vector3>();
        wallConfig.DownWalls = new List<Vector3>();
        wallConfig.Space = 7;

        int counter = 0;

        for (int i = 0; i < settings.ChunkLimit; i += settings.ChunkLimit)
        {
            for (int x = 0; x < settings.ChunkLimit; x++)
            {
                wallConfig.UpWalls.Add(new Vector3(currentX + i + x + 1, currentY, 0));
            }

            for (int x = 0; x < settings.ChunkLimit; x++)
            {
                wallConfig.DownWalls.Add(new Vector3(currentX + i + x + 1, currentY, 0));
            }

            int direction = Random.value > 0.5f ? -1 : 1;
            currentY += Random.Range(2.0f, 4.0f) * direction;

            PrepareCurve(settings.ChunkLimit, wallConfig.UpWalls, counter, -1);
            PrepareCurve(settings.ChunkLimit, wallConfig.DownWalls, counter, -1);
            PreparePerlinNoise(settings.ChunkLimit, wallConfig.UpWalls, counter);
            PreparePerlinNoise(settings.ChunkLimit, wallConfig.DownWalls, counter);

            counter++;
        }

        for (int i = 0; i < settings.ChunkLimit; i++)
        {
            float distance = Mathf.Abs(wallConfig.UpWalls[i].y - wallConfig.DownWalls[i].y);

            if (distance < wallConfig.Space)
            {
                float reducedDistance = (wallConfig.Space - distance);
                wallConfig.UpWalls[i] += new Vector3(0, reducedDistance * 0.7f, 0);
                wallConfig.DownWalls[i] -= new Vector3(0, reducedDistance * 0.8f, 0);
            }
        }

        wallConfig.LastX = currentX + settings.ChunkLimit;
        wallConfig.LastY = currentY;

        return wallConfig;
    }
    private IEnumerator Loop(Chunk chunk)
    {
        GameObject chunkHolder = new GameObject();
        chunkHolder.name = "chunk";

        for (int i = 0; i < chunk.UpWalls.Count; i++)
        {
            InstantiateWalls(chunk.UpWalls, chunkHolder.transform, i, false);
            InstantiateWalls(chunk.DownWalls, chunkHolder.transform, i, true);

            yield return new WaitForEndOfFrame();
        }
    }

    private void PrepareCurve(int chunkLimit, List<Vector3> points, int counter, int w)
    {
        List<Vector3> segmentWalls = points.Skip(chunkLimit * counter).Take(chunkLimit).ToList();
        points.RemoveRange(chunkLimit * counter, chunkLimit);
        segmentWalls = ApplyCurve(w, segmentWalls);
        points.AddRange(segmentWalls);
    }

    private void PreparePerlinNoise(int chunkLimit, List<Vector3> points, int counter)
    {
        List<Vector3> segmentWalls = points.Skip(chunkLimit * counter).Take(chunkLimit).ToList();
        points.RemoveRange(chunkLimit * counter, chunkLimit);
        segmentWalls = ApplyPerlinNoise(segmentWalls, 1);
        points.AddRange(segmentWalls);
    }

    private void InstantiateWalls(List<Vector3> points, Transform parent, int i, bool rotation)
    {
        GameObject cloneObject = Instantiate(WallPrefab, points[i], Quaternion.identity, parent.transform);
        cloneObject.GetComponent<MeshRenderer>().material.color = i % 2 == 0 ? mapSettings.darkColor : mapSettings.lightColor;
        cloneObject.name = i.ToString();
        cloneObject.transform.localEulerAngles = new Vector3(rotation ? 180 : 0, 0, 0);
    }

    private List<Vector3> ApplyPerlinNoise(List<Vector3> wallsPoints, int oriantation)
    {
        float currentY = 0;

        for (int i = 0; i < wallsPoints.Count; i++)
        {
            currentY = Mathf.PerlinNoise(wallsPoints[i].x, currentY);
            float height = currentY * multiplier;

            wallsPoints[i] += new Vector3(0, height * (float)oriantation, 0);
        }

        return wallsPoints;
    }

    private List<Vector3> ApplyCurve(int oriantation, List<Vector3> wallPoints)
    {
        int pieceCount = wallPoints.Count / 20;
        for (int p = 0; p < pieceCount; p++)
        {
            List<Vector3> pieceOfWalls = wallPoints.Skip(p * 20).Take(20).ToList();
            wallPoints.RemoveRange(p * 20, 20);
            int halfPointsCount = pieceOfWalls.Count / 2;

            for (byte i = 0; i < 2; i++)
            {
                List<Vector3> wallPositions = pieceOfWalls.Skip(i * halfPointsCount).Take(halfPointsCount).ToList();
                Vector3 startPoint = wallPositions.First();
                Vector3 endPoint = wallPositions.Last();

                List<Vector3> points = BezierCurve.CalculateCurve(
                    startPoint,
                    startPoint + new Vector3(0, 0.5f * oriantation, 0),
                    endPoint,
                    endPoint + new Vector3(0, 0.5f * oriantation, 0),
                    wallPositions.Count);

                int indexValue = 0;
                for (int x = i * halfPointsCount; x < (i * halfPointsCount) + halfPointsCount; x++)
                {
                    pieceOfWalls[x] = new Vector3(pieceOfWalls[x].x, points[indexValue].y * oriantation, 0);
                    indexValue++;
                }
            }

            wallPoints.AddRange(pieceOfWalls);
        }

        return wallPoints;
    }
}