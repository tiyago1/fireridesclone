﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pool;

public class Wall : MonoBehaviour, IResettable
{
    [SerializeField] private Renderer renderer;

    public void Reset()
    {

    }

    public void SetColor(Color color)
    {
        renderer.material.color = color;
    }
}
