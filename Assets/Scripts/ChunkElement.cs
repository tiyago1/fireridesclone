﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Pool;

public class ChunkElement : MonoBehaviour, IResettable
{
    [SerializeField] private Color defaultColor = Color.white;

    [SerializeField] private Color firstColor;
    [SerializeField] private Color secondColor;

    [SerializeField] private Wall wallPrefab;
    private Dictionary<byte, List<Wall>> walls;
    private Transform trigger;

    private bool isInitialized;

    private void Initialize(MapSettings mapSettings)
    {
        walls = new Dictionary<byte, List<Wall>>();

        for (byte c = 0; c < 2; c++)
        {
            walls.Add(c, this.transform.GetChild(c).GetComponentsInChildren<Wall>().ToList());
            //walls[c].RemoveAt(0);

            if (walls[c].Count < mapSettings.ChunkLimit)
            {
                for (int i = 0; i < mapSettings.ChunkLimit - walls[c].Count; i++)
                {
                    Transform lastElement = walls[c].Last().transform;
                    Wall cloneWall = Instantiate(wallPrefab, this.transform);
                    cloneWall.transform.localPosition = lastElement.localPosition + new Vector3(lastElement.localScale.x, 0, 0);
                    cloneWall.transform.localRotation = lastElement.localRotation;
                    walls[c].Add(cloneWall);
                }
            }
        }

        firstColor = mapSettings.lightColor;
        secondColor = mapSettings.darkColor;
        trigger = this.transform.GetChild(2).transform;

        isInitialized = true;
    }

    public void SetupChunk(Chunk chunk, MapSettings mapSettings)
    {
        if (!isInitialized)
            Initialize(mapSettings);

        ConfigureWalls(chunk.UpWalls, walls[0]);
        ConfigureWalls(chunk.DownWalls, walls[1]);
        this.gameObject.SetActive(true);
    }

    private void ConfigureWalls(List<Vector3> points, List<Wall> walls)
    {
        for (int i = 0; i < points.Count; i++)
        {
            walls[i].transform.localPosition = points[i];
            walls[i].SetColor(i % 2 == 0 ? firstColor : secondColor);
        }
        Test();
    }

    public void Test()
    {
        int certainWallDownIndex = walls[1].Count / 2;
        trigger.localPosition = walls[1][certainWallDownIndex].transform.localPosition;
    }

    public void Reset()
    {

    }
}