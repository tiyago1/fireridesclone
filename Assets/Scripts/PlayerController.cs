﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    private const float DEFAULT_DRAG = 8.3f;

    private readonly Vector3 DEFAULT_RAY_DIRECTION = new Vector3(0.3f, 1.0f);

    [SerializeField] private LineRenderer lineRenderer;
    [SerializeField] private Transform wall;
    [SerializeField] private float speed;

    public Text velocityText;

    private Rigidbody rigidbody;

    private bool isSearchWall;
    private bool isFoundedWall;

    public float multiple = 1;

    public float t;

    private void Awake()
    {
        rigidbody = this.GetComponent<Rigidbody>();
        rigidbody.useGravity = false;
    }

    private void Update()
    {
        this.transform.rotation = Quaternion.LookRotation(rigidbody.velocity);
        velocityText.text = (int)(1f / Time.unscaledDeltaTime) + "fps - magnitude" + rigidbody.velocity.magnitude;
        Camera.main.transform.position = new Vector3(this.transform.position.x - 5, this.transform.position.y, -14.73f);

        if (Input.GetKeyDown(KeyCode.S))
        {
            SetupStarterRope();
        }


        if (Input.GetKey(KeyCode.E))
        {
            Time.timeScale += 0.1f;
        }

        if (Input.GetKey(KeyCode.Q))
        {
            Time.timeScale -= 0.1f;
        }



        if (Input.GetMouseButtonDown(0))
        {
            isSearchWall = true;
        }

        if (isSearchWall && Input.GetMouseButton(0))
        {
            if (!isFoundedWall)
            {
                wall = GetNearestWall(DEFAULT_RAY_DIRECTION);
            }

            SetupRope(wall);
        }

        if (Input.GetMouseButtonUp(0))
        {
            ClearRope();

            speed += (rigidbody.velocity.normalized * multiple).magnitude;
            //multiple = 1;
            wall = null;
            isSearchWall = false;
            isFoundedWall = false;
            rigidbody.drag = 0;
            rigidbody.useGravity = true;
        }
    }

    private void SetupStarterRope()
    {
        if (!isFoundedWall)
        {
            wall = GetNearestWall(Vector3.up);
        }

        DrawRope();
    }

    private Transform GetNearestWall(Vector3 direction)
    {
        RaycastHit hit;
        Transform foundedWall = null;

        if (Physics.Raycast(this.transform.position, direction, out hit, Mathf.Infinity))
        {
            if (hit.collider.CompareTag("Wall"))
            {
                foundedWall = hit.collider.transform;
            }
        }

        isFoundedWall = foundedWall != null;

        return foundedWall;
    }

    private void DrawRope()
    {
        lineRenderer.enabled = true;
        lineRenderer.SetPosition(0, this.transform.position);
        lineRenderer.SetPosition(1, wall.position);
    }

    private void ClearRope()
    {
        lineRenderer.enabled = false;
    }

    private void SetupRope(Transform wall)
    {
        if (wall == null)
            return;

        DrawRope();
        rigidbody.drag = DEFAULT_DRAG;
        rigidbody.useGravity = false;
        Vector3 cross = Vector3.Cross(Vector3.forward, (transform.position - wall.position).normalized).normalized;
        //rigidbody.AddForce(cross * speed * Time.deltaTime, ForceMode.Force);
        multiple += 0.3f * Time.deltaTime;
        rigidbody.velocity = ((cross * speed * Time.deltaTime)); //420
    }

    public void Restart()
    {
        SceneManager.LoadScene(0);
    }
}